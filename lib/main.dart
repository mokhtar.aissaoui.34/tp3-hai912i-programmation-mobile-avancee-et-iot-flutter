import 'package:exo2/presentation/Pages/quiz.cubit.page.dart';
import 'package:exo2/business_logic/bloc/addquestion.cubit.dart';
import 'package:exo2/business_logic/bloc/quiz.cubit.dart';
import 'package:exo2/package data/models/question.model.dart';
import 'package:exo2/package data/models/quiz.model.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package data/DataProvider/QuestionFirebase.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  try {
    QuestionFirebase.getAllQuestion().then((value) => runApp(MyApp(
          resu: value,
        )));
  } catch (e) {}
}

class MyApp extends StatefulWidget {
  List<Question> resu;
  MyApp({
    Key? key,
    required this.resu,
  }) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState(resu);
}

class _MyAppState extends State<MyApp> {
  List<Question> resu;
  _MyAppState(this.resu);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
            create: (context) => QuizCubit(quizModel(questions: resu))),
        BlocProvider(
            create: (context) => addquestion(Question(
                  questiontext: "",
                  isCorrct: false,
                )))
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.grey,
        ),
        home: const QuizCubitPage(),
      ),
    );
  }
}
