// ignore_for_file: file_names

import 'package:cloud_firestore/cloud_firestore.dart';
// ignore: unused_import
import 'package:exo2/package data/models/question.model.dart';

final FirebaseFirestore firestore = FirebaseFirestore.instance;

class QuestionFirebase {
  static CollectionReference getGroupCollection() {
    return firestore.collection('Question');
  }

  static Future<List<Question>> getAllQuestion() async {
    final questionDoc = await getGroupCollection().get();
    return questionDoc.docs
        .map((e) => new Question(
            questiontext: e["questiontext"], isCorrct: e["isCorrct"]))
        .toList();
  }

  static void addquestion(String text, bool isC) async {
    getGroupCollection()
        .add(Question(questiontext: text, isCorrct: isC).toJson());
  }
}
