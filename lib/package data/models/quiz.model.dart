import 'package:exo2/package data/models/question.model.dart';
import 'package:flutter/material.dart';

class quizModel {
  int _score = 0;
  int _index = 0;
  bool _isButtonDisabled = false;
  Color _backtext = Colors.transparent;
  List<Question> questions;
  quizModel({required this.questions});
  void setindex(int index) {
    _index = index;
  }

  void addscore(int index) {
    _score += index;
  }

  void setDisable(bool b) {
    _isButtonDisabled = b;
  }

  void setBackText(Color backText) {
    _backtext = backText;
  }

  List<Question> getQuestion() {
    return questions;
  }

  int getindex() {
    return _index;
  }

  int getscore() {
    return _score;
  }

  int getQuestionCount() {
    return questions.length;
  }

  bool getisButtonDisabled() {
    return _isButtonDisabled;
  }

  void viewScore() {
    questions[_index].questiontext =
        "votre score est : $_score /" + questions.length.toString();
  }

  Color getbacktext() {
    return _backtext;
  }
}
