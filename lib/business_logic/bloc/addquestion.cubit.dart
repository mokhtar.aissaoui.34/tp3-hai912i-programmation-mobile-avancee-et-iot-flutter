import 'package:bloc/bloc.dart';
import 'package:exo2/business_logic/bloc/quiz.cubit.dart';
import 'package:exo2/package data/models/question.model.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// ignore: camel_case_types
class addquestion extends Cubit<Question> {
  addquestion(Question initialState) : super(initialState);
  void btnswitch(bool value) async {
    Question t = Question(questiontext: state.questiontext, isCorrct: value);
    emit(t);
  }

  void updateQuestion(String text) {
    Question t = Question(questiontext: text, isCorrct: state.isCorrct);
    emit(t);
  }

  void restart(BuildContext context1) {
    context1.read<QuizCubit>().restart();
    emit(Question(questiontext: "", isCorrct: false));
  }
}
